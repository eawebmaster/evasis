<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Posts;
class WelcomeController extends Controller
{
    public function index(){
        $posts = Posts::all();
        return view('welcome')->with('posts', $posts);
    }
}
