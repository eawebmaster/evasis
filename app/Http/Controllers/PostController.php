<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Posts;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Auth;
class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $posts = Posts::all();
        return view('posts.index', compact([
            'posts'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            'post_title' => 'required',
            'post_description' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('post/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {

            $post = new Posts();
            $post->post_title = $request->post('post_title');
            $post->post_description = $request->post('post_description');
            $post->user_id = Auth::id();
            $post->save();


            Session::flash('message', 'Gönderi Başarıyla Eklendi');
            return Redirect::to('post');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $row = Posts::find($id);
        return view('posts.edit', compact([
            'row'
        ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'post_title' => 'required',
            'post_description' => 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::to('post/create')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {

            $post = Posts::find($id);
            $post->post_title = $request->post('post_title');
            $post->post_description = $request->post('post_description');
            $post->user_id = Auth::id();
            $post->save();


            Session::flash('message', 'Gönderi Başarıyla Güncellendi');
            return Redirect::to('post');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Posts::find($id)->delete();
        Session::flash('message', 'Gönderi Başarıyla Güncellendi');
        return Redirect::to('post');
    }
}
