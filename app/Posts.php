<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
class Posts extends Model
{
    protected $table = 'posts';

    public function user(){
       return $this->belongsTo('App\User');
    }
}
