@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br />
                        @endif
                            <form method="post" action="{{ route('post.update', $row->id) }}">
                                @method('PATCH')
                            <div class="form-group">
                                @csrf
                                <label for="name">Başlık:</label>
                                <input type="text" class="form-control" value="{{$row->post_title}}" name="post_title"/>
                            </div>
                            <div class="form-group">
                                <label for="price">Açıklama :</label>
                                <input type="text" value="{{$row->post_description}}" class="form-control" name="post_description"/>
                            </div>

                            <button type="submit" class="btn btn-primary">Güncelle</button>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
