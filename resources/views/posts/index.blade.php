@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Gönderiler <a href="{{route('post.create')}}" class="btn btn-primary">Yeni
                            Gönderi Ekle</a></div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if (Session::has('message'))
                            <div class="alert alert-info">{{ Session::get('message') }}</div>
                        @endif

                        @if(count($posts) > 0)
                            <table class="table-bordered table-striped table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Başlık</th>
                                    <th>Açıklama</th>
                                    <th colspan="2">İşlem</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($posts as $post)
                                    <tr>
                                        <td>{{$post->id}}</td>
                                        <td>{{$post->post_title}}</td>
                                        <td>{{$post->post_description}}</td>
                                        <td><a href="{{route('post.edit', ['id'=>$post->id])}}">Güncelle</a></td>
                                        <td><a href="{{route('post.edit', ['id'=>$post->id])}}">Sil</a></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
