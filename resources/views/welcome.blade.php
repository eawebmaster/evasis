<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #dce0ce;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .card {
            background: #ffffff;
            transition: 0.3s;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            border-radius: 5px;
            width: 40%;
            margin-left: 5px;
            float: left;


        }

        .card-body {
            padding: 10px 5px;
        }

        .title {
            font-size: 84px;
        }

        .links > a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }
    </style>
</head>
<body>
<div class="flex-center position-ref full-height">
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <a href="{{ url('/home') }}">Anasayfa</a>
            @else
                <a href="{{ route('login') }}">Giriş Yap</a>

                @if (Route::has('register'))
                    <a href="{{ route('register') }}">Kayıt ol</a>
                @endif
            @endauth
        </div>
    @endif

    <div class="content">
        @isset($posts)
            @foreach($posts as $post)
                <div class="card" style="width: 18rem;">

                    <div class="card-body">
                        <h5 class="card-title">{{$post->post_title}}</h5>
                        <p class="card-text">{{$post->post_description}}</p>
                        <a href="#" class="btn btn-primary">{{$post->user->name}}</a>

                    </div>
                </div>
            @endforeach
        @endisset
    </div>
</div>
</body>
</html>
